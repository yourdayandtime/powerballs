
CREATE TABLE `customers` (
  `customer_id` int NOT NULL,
  `name` varchar(20) NOT NULL
);


INSERT INTO `customers` (`customer_id`, `name`) VALUES
(1, 'John Smith'),
(2, 'Johnas Smith'),
(3, 'Joanna Smit'),
(4, 'Mark Smiths');

CREATE TABLE `customer_products` (
  `customer_id` int NOT NULL,
  `product_id` int NOT NULL,
  `purchase_date` date NOT NULL
);

INSERT INTO `customer_products` (`customer_id`, `product_id`, `purchase_date`) VALUES
(1, 1, '2016-12-12'),
(1, 3, '2016-12-13'),
(2, 4, '2016-12-12'),
(4, 2, '2016-12-14');

CREATE TABLE `draws` (
  `id` int NOT NULL,
  `main_numbers` json NOT NULL,
  `power_numbers` json DEFAULT NULL,
  `drawn_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `products` (
  `product_id` int NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `product_category` varchar(20) NOT NULL,
  `cost` int NOT NULL
);

INSERT INTO `products` (`product_id`, `product_name`, `product_category`, `cost`) VALUES
(1, 'Green mouse', 'Personal computers', 5),
(2, 'Blue cup', 'Kitchen essentials', 10),
(3, 'Tea bags', 'Kitchen essentials', 8),
(4, 'Smart phone ', 'Mobile phones', 50);

ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

ALTER TABLE `draws`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

ALTER TABLE `customers`
  MODIFY `customer_id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `draws`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;


ALTER TABLE `products`
  MODIFY `product_id` int NOT NULL AUTO_INCREMENT;
COMMIT;
