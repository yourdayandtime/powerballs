
<?php
/** To be extended by both mainballs and powerballs classes**/
abstract class BallsAbstract {

    public function getBallOptions($min = 1, $max = 49){

        //get balls from minimum to maximum range
        $options_range = range($min, $max);

        //pick a random key from array values
        $key = array_rand($options_range);

        $range_limit = $options_range[$key]; // get value by key

        //get new  random range no less than 1 and not greater than $range_limit
        $options_range = range(1, $range_limit);

        return $options_range;
    }


    public function getDrawOptions($min = 1, $max = 3){

       return range($min , $max);
   }
}
?>