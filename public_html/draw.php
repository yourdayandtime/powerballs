<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once './conf/db.php';

    require_once 'main_balls.php';

    require_once 'power_balls.php';

    $main_balls_Obj = new MainBalls();

    $power_balls_Obj = new PowerBalls();

    $main_draws = $main_balls_Obj->draw();

    $power_draws = $power_balls_Obj->draw();

    $m = json_encode($main_draws);

    $p = $power_draws ? json_encode($power_draws) : NULL ;

    $stmt = $db->prepare("INSERT INTO draws (main_numbers, power_numbers) VALUES(?,?)");

    $stmt->bind_param("ss", $m, $p);

    /* execute prepared statement */
    $stmt->execute();
    $inserted = $stmt->insert_id;
    /* close statement */
    $stmt->close();

    $stmt = $db->prepare("SELECT drawn_on FROM draws where id = ? LIMIT 1");

    $stmt->bind_param("d", $inserted);

    $stmt->execute();

    $result = $stmt->get_result(); // stmt response

    $row = $result->fetch_assoc();

    /* close statement */
    $stmt->close();
    /* close bd connection */
    $db->close();

    $power_draws = is_array($power_draws) ? implode(" - ", $power_draws) : [];

    $data = array('main_draws' => implode(" - ", $main_draws), 'power_draws' => $power_draws);

    if (sizeof($row)) {

        $data[ 'drawn_on'] = date("d/m/Y h:i", strtotime($row['drawn_on']));
    }

    echo json_encode($data);

    ?>