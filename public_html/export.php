<?php
    //include db conf file
    require_once './conf/db.php';

    $result = $db->query("SELECT * FROM draws ORDER BY drawn_on ASC limit 100");

    if($result->num_rows > 0){

        $delimiter = ",";

        $filename = "latest_100_powerballs_winnin_numbers_" . date('Y-m-d h:i:s') . ".csv";

        //create a file pointer
        $f = fopen('php://memory', 'w');

        //set headers
        $fields = array('Main Draws', 'Power Balls', 'Date');

        fputcsv($f, $fields, $delimiter);

        $rows = $result->fetch_all();

        //write to file pointer
        foreach($rows as $row) {

            $lineData = array(implode(" - ", json_decode($row[1])), implode(" - ", json_decode($row[2])), date("d/m/Y h:i",strtotime($row[3])));

            fputcsv($f, $lineData, $delimiter);
        }

        //back to beginning of file
        fseek($f, 0);

        //set headers to download file rather than displayed
        header('Content-Type: text/csv');

        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        //output all remaining data on a file pointer
        fpassthru($f);
    } else {
        header('Content-Type: text/csv');

        header('Content-Disposition: attachment; filename="no_draws_yet.csv";');
    }
    exit(); ?>