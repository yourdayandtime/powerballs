<!DOCTYPE html>
<html lang="en">
<head>
  <title>Power Balls</title>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link
  rel="stylesheet"
  href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
  integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
  crossOrigin="anonymous"
  />
</head>
<body>
    <style>
        ul li {
            display: inline;
            margin: 2px;
            padding: 8px;
        }
        ul  {
            min-width: 696px;
            list-style: none;
            padding-top: 20px;
        }
        table td, .table th {
            padding: 5px !important;
        }
    </style>

    <?php

    require_once './conf/db.php';

    $result = $db->query("SELECT * FROM draws ORDER BY id DESC LIMIT 10");

    $rows = [];

    if ($result->num_rows) {

        $rows = $result->fetch_all();
    }

    /* close bd connection */
    $db->close();

    ?>
    <div class="container"> 
        <div class="col-md-12 text-center mx-auto mt-4">
            <h1>Lotto Draw Machine</h1>
        </div>
        <div class="col-md-8 text-center mx-auto">
            <table class="table table-striped" id="draws_table">
                <thead>
                    <tr>
                        <th scope="col">Main Balls</th>
                        <th scope="col">Power Balls</th>
                        <th scope="col">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(sizeof($rows)) {

                            foreach($rows as $row) {
                                
                                $pw_balls = ($row[2] != null ? implode(" - ", json_decode($row[2])) :'');
                                echo "<tr class='tr-draw'><td>". implode(" - ", json_decode($row[1])) . "</td>";
                                echo "<td>". $pw_balls . "</td>";
                                echo "<td><small>". date("d/m/Y h:i", strtotime($row[3])) . "</small></td></tr>";
                            } 

                        }  else {
                            echo "<tr class='no-draw text-warning text-center'><td colspan='3'>No draws made yet. Be the first to play.</td></tr>";
                        } 
                    ?>

                </tbody>
            </table>
        </div>
        <div class="col-md-9 text-center mx-auto">
            <div class="col-md-4">
                <div id="main_draws"></div>
            </div>
            <div class="col-md-4">
                <div id="power_draws"></div>
            </div>
        </div>
        <div class="col-md-8 text-center mx-auto">

            <a href="#" class="btn btn-success btn-lg m-4" id="draw">
                Play
            </a>

        </div>
        <div class="col-md-6 mx-auto text-center m-4 p-4">
            Powerball Draws Nov 2020
        </div>
    </div>
    <script
    src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossOrigin="anonymous"
    ></script>
    <script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
    crossOrigin="anonymous"
    ></script>
    <script src="/js/draws.js"></script>
</body>
</html>