/*** 1 - Total number of purchases, the Total amount spent
and the Average spent by each customer, regardless of whether purchases were made.***/
SELECT cust.name, count(cust_prod.product_id) as purchases, COALESCE(SUM(pd.cost),0) total_spent, AVG(pd.cost) average_spent
from customers cust left join customer_products cust_prod on cust.customer_id = cust_prod.customer_id left join products pd on cust_prod.product_id = pd.product_id
GROUP BY cust.name
order by total_spent desc;

/** 2 - Total number of purchase made per day, the Total amount
spent per day, and the maximum and minimum of purchases per day**/
SELECT totals.purchase_date, totals.total_purchase, min(purch.total_purchase) min_purchase, max(purch.total_purchase) max_purchase, sums.total_amount_spent
from (select COUNT(cust_prod.product_id) total_purchase, cust_prod.purchase_date
    FROM customer_products as cust_prod
    GROUP by cust_prod.purchase_date) as purch JOIN (select COUNT(cust_prod.product_id) total_purchase, cust_prod.purchase_date
    FROM customer_products as cust_prod
    GROUP by cust_prod.purchase_date) totals JOIN (select sum(prod.cost) total_amount_spent, cust_prod.purchase_date date_of_purchase
    FROM customer_products as cust_prod LEFT JOIN products as prod on prod.product_id = cust_prod.product_id
    GROUP by cust_prod.purchase_date) as sums on sums.date_of_purchase = totals.purchase_date 
group by sums.total_amount_spent, totals.purchase_date,sums.date_of_purchase;

/*** 3 - Total sales for each product_category and the number
of items sold in that category. ****/
SELECT sales.product_category, count(sales.product_id) item_sold_per_cat, SUM(sales.total_cat_sale) as sales_amount
from (SELECT cust_prod.product_id , p.product_category, sum(p.cost) total_cat_sale
    from customer_products as cust_prod left join( select product_id, product_category, cost
        from products) as p on p.product_id = cust_prod.product_id
    group by p.product_category, cust_prod.product_id) as sales
group by sales.product_category;