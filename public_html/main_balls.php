<?php
    //require interface
    require_once('balls_interface.php');

    //require abstract class
    require_once('balls_abstract.php');

    class MainBalls extends BallsAbstract implements BallsInterface {

        private $_max_balls = 49;

        private $_min_balls = 40;

        private $_max_draw = 7;

        private $_min_draw =  5;

        public function draw() {

            $balls_array = $this->getBallOptions($this->_min_balls, $this->_max_balls);

            $drawOptions = $this->getDrawOptions($this->_min_draw, $this->_max_draw);

            $key = array_rand($drawOptions);

            $times = (int) $drawOptions[$key];

            $draws = [];

            for($i = 1; $i <= $times; $i++) {

                $key = array_rand($balls_array);

                $draws[] = $balls_array[$key];

                //unset to avoid duplication
                unset($balls_array[$key]);
            }

            if (count($draws)) {

                return $draws;
            }
        }
    }
?>