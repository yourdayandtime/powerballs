
<?php
	/** To be implemented by both mainballs and powerballs classes**/
	interface BallsInterface {

		public function getBallOptions();

		public function getDrawOptions();

		public function draw();
	}
?>