 $(document).ready(function(){

        var req_obj = new XMLHttpRequest();
        
        addExportBtn();//add export btn if any draw exists
       
            
        $("#draw").on('click', function(e) {

            e.preventDefault();
            //console.log('lucky you');
            req_obj.addEventListener("error", transferFailed);
            req_obj.open("GET", "draw.php");
            req_obj.send();

            req_obj.onreadystatechange = function() {

                if (req_obj.readyState === 4) {
                    //console.log(req_obj.response);
                    var response = JSON.parse(req_obj.response);
                    //console.log(response);
                    var main_draws = "main_draws" in response ? response.main_draws : "";
                    var power_draws = "power_draws" in response ? response.power_draws : "";
                    var drawn_on = "drawn_on" in response ? response.drawn_on : "";
                    var tr = "<tr class='latest tr-draw'><td class='text-success'>" + main_draws + "</td><td class='text-warning'>" + power_draws + "</td><td><small>" + drawn_on+ "</small></td></tr>";
                    $("table#draws_table").find('tbody').prepend(tr);
                    addExportBtn();//add draw btn if not present
                }
            }
        })
            
        $("#export").on('click', function() {
            $("button#export").attr('disabed', true);
            req_obj.addEventListener("progress", updateProgress);
            req_obj.open("GET", "export.php");
            req_obj.send();
            }
        )
        // progress on transfers from the server to the client 
        function updateProgress (oEvent) {
            if (oEvent.lengthComputable) {
                var percentComplete = oEvent.loaded / oEvent.total * 100;
                console.log(percentComplete);
            // could be displayed somewhere
            }
        }

        function transferFailed(evt) {
            console.log("An error occurred while transferring the file.");
        }

        function addExportBtn() {

            var exp_btn = '<a href="export.php"  class="btn btn-warning btn-lg m-4" id="export">Export All</a>';
            var exp_id = document.getElementById('export');
            var tr_draws = $(document).find('tr.tr-draw');

            //add export btn when the page loads
            //and draws are available     
            if (!exp_id) {
                
                if (tr_draws && tr_draws.length) {
                    $('#draw').after(exp_btn);
                }
            }

            var no_draw = $(document).find('tr.no-draw');
            var exp_id = document.getElementById('export');
            //remove 'no draw yet message' if any
            if (exp_id && tr_draws && no_draw && no_draw.length) {
                $(no_draw).remove();
            }
        }
    })

    